# 环境篇

## 课件资料 下载

GitHub（ 地址：https://github.com/ ）是一个面向开源及私有软件项目的托管平台（国外）

### 在GitHub注册账号

要想使用GitHub的相关服务，需要注册账号（地址： https://github.com/ ）

**第一步：登录网址，点击sign up注册账号**

![image-20200420105151475](../img-folder/images/image-1.png)





**第二步：填写信息，注意邮箱要真实有效**



![image-20200420111044052](../img-folder/images/image-2.png)





**第三步:直接点击join a free plan**

![image-20200420111151656](../img-folder/images/image-3.png)



**第四步：直接划到最下面点击complete setup**

![image-20200420111412775](../img-folder/images/image-4.png)



**第五步：邮箱需要验证，验证完成后登陆进github**

![image-20200420112207590](../img-folder/images/image-5.png)



**验证邮箱：进入邮箱后点击按钮，进行页面跳转**

![image-20200420113200765](../img-folder/images/image-6.png)



**跳转页面后：点击skip this for now**

![image-20200420113304435](../img-folder/images/image-7.png)



## 创建仓库

**第一步：点击头像**

![image-20200420113422355](../img-folder/images/image-9.png)



**第二步：点击create Repository（创建仓库）**

![image-20200420113517381](../img-folder/images/image-10.png)



**第三步：填写仓库信息**

![image-20200420113828310](../img-folder/images/image-11.png)





**第四步：跳转到仓库主页，记下自己的仓库链接，这个是要提交到拉勾学习平台的作业地址的：**



![image-20200518181125731](../img-folder/images/image-54.png)



**第五步：在学习平台视频观看完成后点击提交作业**

![image-20200420115109670](../img-folder/images/image-17.png)

![image-20200420115052943](../img-folder/images/image-18.png)



**第六步：将github仓库地址填入作业网址中**



![image-20200420115431217](../img-folder/images/image-20.png)

##### 第七步：作业如何上传到github仓库，请参考链接：

  https://gitee.com/lagouedu/Basic-document/blob/master/document/Git_basic.md